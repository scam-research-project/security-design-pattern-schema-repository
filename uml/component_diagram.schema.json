{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "component-diagram/1.0.0",
    "title": "UML Component Diagram",
    "type": "object",
    "properties": {
        "diagram_metadata": {
            "type": "object",
            "properties": {
                "diagram_type": {
                    "type": "string",
                    "const": "component"
                },
                "version": {
                    "type": "string",
                    "const": "1.0.0"
                }
            },
            "required": [
                "diagram_type",
                "version"
            ]
        },
        "name": {
            "type": "string"
        },
        "description": {
            "type": "string"
        },
        "components": {
            "type": "array",
            "description": "List of components in the UML component diagram.",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "description": "The unique identifier of the component."
                    },
                    "name": {
                        "type": "string",
                        "description": "The name of the component."
                    },
                    "stereotype": {
                        "type": "string",
                        "description": "The stereotype of the component."
                    },
                    "ports": {
                        "type": "array",
                        "description": "List of ports of the component.",
                        "items": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "string",
                                    "description": "The unique identifier of the port."
                                },
                                "required_interfaces": {
                                    "type": "array",
                                    "description": "List of required interfaces.",
                                    "items": {
                                        "type": "string",
                                        "description": "The id of a required interface."
                                    }
                                },
                                "provided_interfaces": {
                                    "type": "array",
                                    "description": "List of provided interfaces.",
                                    "items": {
                                        "type": "string",
                                        "description": "The id of a provided interface."
                                    }
                                }
                            },
                            "required": [
                                "id"
                            ]
                        }
                    }
                },
                "required": [
                    "id",
                    "name",
                    "ports"
                ]
            }
        },
        "artifacts": {
            "type": "array",
            "description": "List of artifacts in the UML component diagram.",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "description": "The unique identifier of the artifact."
                    },
                    "name": {
                        "type": "string",
                        "description": "The name of the artifact."
                    },
                    "stereotype": {
                        "type": "string",
                        "enum": [
                            "artifact",
                            "specification"
                        ],
                        "description": "The stereotype of the artifact."
                    }
                },
                "required": [
                    "id",
                    "name",
                    "stereotype"
                ]
            }
        },
        "classes": {
            "type": "array",
            "description": "List of classes in the UML component diagram.",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "description": "The unique identifier of the class."
                    },
                    "name": {
                        "type": "string",
                        "description": "The name of the class."
                    }
                },
                "required": [
                    "id",
                    "name"
                ]
            }
        },
        "interfaces": {
            "type": "array",
            "description": "List of interfaces in the UML component diagram.",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "description": "The unique identifier of the interface."
                    }
                },
                "required": [
                    "id"
                ]
            }
        },
        "connectors": {
            "type": "array",
            "description": "List of connectors in the UML component diagram.",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "description": "The unique identifier of the connector."
                    },
                    "source": {
                        "type": "string",
                        "description": "The id of the source port."
                    },
                    "target": {
                        "type": "string",
                        "description": "The id of the target port."
                    }
                },
                "required": [
                    "id",
                    "source",
                    "target"
                ]
            }
        },
        "relationships": {
            "type": "array",
            "description": "List of relationships in the UML component diagram.",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "description": "The unique identifier of the relationship."
                    },
                    "type": {
                        "type": "string",
                        "enum": [
                            "use",
                            "manifest",
                            "dependency",
                            "delegation"
                        ],
                        "description": "The type of the relationship."
                    },
                    "source": {
                        "type": "string",
                        "description": "The id of the source artifact, class, component, port, or interface."
                    },
                    "target": {
                        "type": "string",
                        "description": "The id of the target artifact, component, port, or interface."
                    }
                },
                "required": [
                    "id",
                    "type",
                    "source",
                    "target"
                ]
            }
        }
    },
    "required": [
        "diagram_metadata",
        "name",
        "components",
        "interfaces"
    ],
    "additionalProperties": false
}
