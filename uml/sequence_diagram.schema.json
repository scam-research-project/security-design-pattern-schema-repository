{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "sequence-diagram/1.0.0",
    "title": "UML 2 Sequence Diagram Metamodel",
    "type": "object",
    "properties": {
        "diagram_metadata": {
            "type": "object",
            "properties": {
                "diagram_type": {
                    "type": "string",
                    "const": "sequence"
                },
                "version": {
                    "type": "string",
                    "const": "1.0.0"
                }
            },
            "required": [
                "diagram_type",
                "version"
            ]
        },
        "name": {
            "type": "string"
        },
        "description": {
            "type": "string"
        },
        "participants": {
            "type": "array",
            "description": "List of participants in the sequence diagram.",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string",
                        "description": "The id of the participant. Used to uniquely identify sender and receiver of a message."
                    },
                    "name": {
                        "type": "string",
                        "description": "The name of the participant."
                    },
                    "type": {
                        "type": "string",
                        "description": "The type of the participant. Can be any of: 'actor', 'object'.",
                        "enum": [
                            "actor",
                            "object"
                        ]
                    }
                },
                "required": [
                    "id",
                    "name",
                    "type"
                ]
            }
        },
        "messages": {
            "type": "array",
            "description": "List of messages exchanged in the sequence diagram.",
            "items": {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "The name of the message."
                    },
                    "sender": {
                        "type": "string",
                        "description": "The participant id of the sender of the message."
                    },
                    "receiver": {
                        "type": "string",
                        "description": "The participant id of the receiver of the message."
                    },
                    "type": {
                        "type": "string",
                        "enum": [
                            "synchronous",
                            "asynchronous",
                            "reply"
                        ],
                        "description": "The type of the message."
                    },
                    "sequence_number": {
                        "type": "integer",
                        "description": "The order of the message in the sequence."
                    }
                },
                "required": [
                    "name",
                    "sender",
                    "receiver",
                    "type",
                    "sequence_number"
                ]
            }
        },
        "fragments": {
            "type": "array",
            "description": "List of interaction fragments in the sequence diagram.",
            "items": {
                "$ref": "#/$defs/fragment"
            }
        }
    },
    "required": [
        "diagram_metadata",
        "name",
        "participants",
        "messages"
    ],
    "additionalProperties": false,
    "$defs": {
        "fragment": {
            "type": "object",
            "properties": {
                "type": {
                    "type": "string",
                    "enum": [
                        "alt",
                        "opt",
                        "loop",
                        "par",
                        "region"
                    ],
                    "description": "The type of interaction fragment."
                },
                "fragments": {
                    "type": "array",
                    "description": "Nested fragments within this fragment.",
                    "items": {
                        "$ref": "#/$defs/fragment"
                    }
                }
            },
            "required": [
                "type"
            ],
            "allOf": [
                {
                    "if": {
                        "properties": {
                            "type": {
                                "enum": [
                                    "opt",
                                    "loop",
                                    "region"
                                ]
                            }
                        }
                    },
                    "then": {
                        "properties": {
                            "type": {
                                "type": "string",
                                "enum": [
                                    "opt",
                                    "loop",
                                    "region"
                                ]
                            },
                            "sequence_numbers": {
                                "type": "array",
                                "description": "List of sequence numbers of type int that are included in this frame.",
                                "items": {
                                    "type": "integer"
                                }
                            },
                            "description": {
                                "type": "string",
                                "description": "A brief description of what this fragment is about"
                            },
                            "fragments": {
                                "type": "array",
                                "description": "Nested fragments within this fragment.",
                                "items": {
                                    "$ref": "#/$defs/fragment"
                                }
                            }
                        },
                        "required": [
                            "sequence_numbers"
                        ],
                        "additionalProperties": false
                    }
                },
                {
                    "if": {
                        "properties": {
                            "type": {
                                "const": "alt"
                            }
                        }
                    },
                    "then": {
                        "properties": {
                            "type": {
                                "type": "string",
                                "const": "alt"
                            },
                            "blocks": {
                                "type": "array",
                                "description": "Blocks within the alt fragment.",
                                "items": {
                                    "type": "object",
                                    "properties": {
                                        "condition": {
                                            "type": "string",
                                            "description": "Condition of the alt block."
                                        },
                                        "sequence_numbers": {
                                            "type": "array",
                                            "description": "Sequence numbers in the alt block.",
                                            "items": {
                                                "type": "integer"
                                            }
                                        }
                                    },
                                    "required": [
                                        "condition",
                                        "sequence_numbers"
                                    ],
                                    "additionalProperties": false
                                }
                            },
                            "fragments": {
                                "type": "array",
                                "description": "Nested fragments within this fragment.",
                                "items": {
                                    "$ref": "#/$defs/fragment"
                                }
                            },
                            "additionalProperties": false
                        },
                        "required": [
                            "blocks",
                            "type"
                        ],
                        "additionalProperties": false
                    }
                },
                {
                    "if": {
                        "properties": {
                            "type": {
                                "const": "par"
                            }
                        }
                    },
                    "then": {
                        "properties": {
                            "type": {
                                "type": "string",
                                "const": "par"
                            },
                            "blocks": {
                                "type": "array",
                                "description": "Blocks within the par fragment.",
                                "items": {
                                    "type": "object",
                                    "properties": {
                                        "sequence_numbers": {
                                            "type": "array",
                                            "description": "Sequence numbers in the par block.",
                                            "items": {
                                                "type": "integer"
                                            }
                                        }
                                    },
                                    "required": [
                                        "sequence_numbers"
                                    ],
                                    "additionalProperties": false
                                }
                            },
                            "fragments": {
                                "type": "array",
                                "description": "Nested fragments within this fragment.",
                                "items": {
                                    "$ref": "#/$defs/fragment"
                                }
                            },
                            "additionalProperties": false
                        },
                        "required": [
                            "blocks"
                        ],
                        "additionalProperties": false
                    }
                }
            ]
        }
    }
}